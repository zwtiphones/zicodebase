//
//  ViewController.m
//  ImageDemo
//
//  Created by Chintan Dave on 27/04/15.
//  Copyright (c) 2015 Chintan Dave. All rights reserved.
//

#import "ZWTCropImageViewController.h"
#import "ZWTTouchTansperentView.h"
#import "UIImage+ZWTImage.h"

@interface ZWTCropImageViewController () <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrvImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgvPhoto;

@property (weak, nonatomic) IBOutlet ZWTTouchTansperentView *viewCropOverlay;
@property (weak, nonatomic) IBOutlet ZWTTouchTansperentView *viewCrop;

@property (weak, nonatomic) IBOutlet UIView *viewBottom;
@property (weak, nonatomic) IBOutlet UIView *viewTop;

@end

@implementation ZWTCropImageViewController

@synthesize scrvImage, imgvPhoto, viewCropOverlay, viewCrop, imgPhoto;
@synthesize viewBottom,viewTop;

#pragma mark - UIViewController Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareViews];
}

#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
     [self centerScrollViewContents];
}

#pragma mark - Event Method
- (IBAction)btnCroptap:(UIBarButtonItem *)sender
{
    CGRect cropRect = [viewCropOverlay convertRect:viewCrop.frame toView:imgvPhoto];
    
    if(cropRect.origin.x < -1 || cropRect.origin.y < -1)
    {
        return;
    }
    
    CGSize imageSize = imgPhoto.size;
    
    CGFloat factor;
    
    if (imageSize.width < imageSize.height)
    {
        factor = (imgPhoto.size.width * imgPhoto.scale) / CGRectGetWidth(self.view.frame);
    }
    else
    {
        factor = (imgPhoto.size.height * imgPhoto.scale) / CGRectGetHeight(scrvImage.frame);
    }
    
    cropRect.origin.x *= factor;
    cropRect.origin.y *= factor;
    
    cropRect.size.width  *= factor;
    cropRect.size.height *= factor;
    
    UIImage *cropedImage = [imgPhoto cropedImagewithCropRect:cropRect];
    
    if(cropedImage.size.width * cropedImage.scale < 100 || cropedImage.size.height * cropedImage.scale < 100)
    {
        NSLog(@"please select image above 100 X 100");
    }
    else
    {
        [self.pickerDelegate imagePickerController:(ZWTImagePickerController *)self.navigationController
                             didFinishPickingMedia:@[cropedImage]];
    }
}

#pragma mark - View Methods
- (void)prepareViews
{
    [imgvPhoto setCenter:CGPointMake(self.scrvImage.bounds.size.width / 2, 0)];

    self.navigationController.navigationBar.translucent = NO;

    scrvImage.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    viewCrop.layer.borderColor = [UIColor whiteColor].CGColor;
    viewCrop.layer.borderWidth = 1.0;
    
    scrvImage.delegate         = self;
    scrvImage.minimumZoomScale = 1.0;
    scrvImage.maximumZoomScale = 3.0;
    
    [self setViewFrame];
    [self loadImage];
    
    scrvImage.zoomScale = 1.01;
    scrvImage.zoomScale = 1.0;
    
    [self centerScrollViewContents];
}

- (void)setViewFrame
{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    CGPoint scrvImageCenter = scrvImage.center;
    
    CGFloat width = MIN(CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    
    scrvImage.frame = CGRectMake(0, 0, width, width);
    scrvImage.center = scrvImageCenter;
    
    viewCropOverlay.frame = self.view.bounds;
    
    viewCrop.frame = scrvImage.frame;

    CGRect frameViewTop;
    CGRect frameViewBottom;

    if(orientation == UIInterfaceOrientationPortrait ||
       orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        frameViewTop.origin.x    = 0;
        frameViewTop.origin.y    = 0;
        frameViewTop.size.width  = CGRectGetWidth(self.view.frame);
        frameViewTop.size.height = CGRectGetMinY(viewCrop.frame);
        
        frameViewBottom.origin.x    = 0;
        frameViewBottom.origin.y    = CGRectGetMaxY(viewCrop.frame);
        frameViewBottom.size.width  = CGRectGetWidth(self.view.frame);
        frameViewBottom.size.height = CGRectGetHeight(frameViewTop);
    }
    else if (orientation == UIInterfaceOrientationLandscapeLeft ||
             orientation == UIInterfaceOrientationLandscapeRight)
    {
        frameViewTop.origin.x    = 0;
        frameViewTop.origin.y    = 0;
        frameViewTop.size.width  = CGRectGetMinX(viewCrop.frame);
        frameViewTop.size.height = CGRectGetHeight(self.view.frame);
        
        frameViewBottom.origin.x    = CGRectGetMaxX(viewCrop.frame);
        frameViewBottom.origin.y    = 0;
        frameViewBottom.size.width  = CGRectGetWidth(frameViewTop);
        frameViewBottom.size.height = CGRectGetHeight(frameViewTop);
    }
    
    viewTop.frame    = frameViewTop;
    viewBottom.frame = frameViewBottom;
}

- (void)loadImage
{
    imgvPhoto.image = imgPhoto;
    
    CGSize imageSize = imgPhoto.size;
    
    NSLog(@"Width : %@, Height : %@", @(imageSize.width), @(imageSize.height));
    
    CGFloat height = (imageSize.height * CGRectGetWidth(self.view.frame)) / imageSize.width;

    if (imageSize.width < imageSize.height)
    {
        CGFloat width  = (imageSize.width * CGRectGetHeight(self.view.frame)) / imageSize.height;
    
        width = CGRectGetWidth(self.view.frame);
        
        imgvPhoto.frame  = CGRectMake(0, 0, width, height);
        imgvPhoto.center = CGPointMake(CGRectGetWidth(scrvImage.frame) / 2.0 , CGRectGetHeight(scrvImage.frame) / 2.0);
    }
    else
    {
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        CGFloat height;
        CGFloat width;
        
        if(orientation == UIInterfaceOrientationPortrait ||
           orientation == UIInterfaceOrientationPortraitUpsideDown)
        {
            height = CGRectGetWidth(self.view.frame);
            width  = (imageSize.width * CGRectGetWidth(self.view.frame)) / imageSize.height;
        }
        else if (orientation == UIInterfaceOrientationLandscapeLeft ||
                 orientation == UIInterfaceOrientationLandscapeRight)
        {
            height = CGRectGetHeight(self.view.frame);
            width  = (imageSize.width * CGRectGetHeight(self.view.frame)) / imageSize.height;
        }
        
        imgvPhoto.frame  = CGRectMake(0, 0, width, height);
        imgvPhoto.center = CGPointMake(CGRectGetWidth(scrvImage.frame) / 2.0 , CGRectGetHeight(scrvImage.frame) / 2.0);
    }
}

#pragma mark - UIScrollViewDelegate Methods
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return imgvPhoto;
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    return NO;
}

#pragma mark - Helper Methods
+ (UIImage *)imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage* img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

- (void)centerScrollViewContents
{
    CGSize boundsSize    = self.scrvImage.bounds.size;
    CGRect contentsFrame = self.imgvPhoto.frame;
    
    if (contentsFrame.size.width < boundsSize.width)
    {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    }
    else
    {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height)
    {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    }
    else
    {
        contentsFrame.origin.y = 0.0f;
    }
    
    self.imgvPhoto.frame = contentsFrame;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
    {
        [self prepareViews];
    }
                                 completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
    {
         
    }];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

@end