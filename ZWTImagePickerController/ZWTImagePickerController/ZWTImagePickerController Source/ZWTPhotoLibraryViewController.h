//
//  PhotoLibraryViewController.h
//  Photocopy
//
//  Created by Vivek on 21/04/15.
//  Copyright (c) 2015 Chintan Dave. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZWTImagePickerController.h"

@import Photos;

@interface ZWTPhotoLibraryViewController : UIViewController

@property (nullable, nonatomic, weak) id <ZWTImagePickerControllerDelegate> pickerDelegate;

@property (strong, nonatomic) PHFetchResult *_Nullable cameraRolls;

@end