//
//  CustomCameraViewController.m
//  Photocopy
//
//  Created by Vivek on 21/04/15.
//  Copyright (c) 2015 Chintan Dave. All rights reserved.
//

#import "ZWTCameraViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "UIImage+ZWTImage.h"
#import "ZWTAlbumViewController.h"
#import "ZWTImagePickerController.h"
#import "ZWTTouchTansperentView.h"

@interface ZWTCameraViewController ()<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *preView;
@property (weak, nonatomic) IBOutlet UIView *upperView;
@property (weak, nonatomic) IBOutlet UIView *lowerView;
@property (weak, nonatomic) IBOutlet UIView *viewCrop;

@property (weak, nonatomic) IBOutlet UIButton *btnCapture;
@property (weak, nonatomic) IBOutlet UIButton *btnRecentImage;
@property (weak, nonatomic) IBOutlet UIButton *btnFlashLight;
@property (weak, nonatomic) IBOutlet UIButton *btnCameraChange;

@property (weak, nonatomic) IBOutlet UIScrollView *scrvCamera;
@property (weak, nonatomic) IBOutlet ZWTTouchTansperentView *viewCropLayer;

@property (strong, nonatomic) AVCaptureSession *session;
@property (strong, nonatomic) AVCaptureDevice *backCamera,*frontCamera;

@property (strong, nonatomic) AVCaptureVideoPreviewLayer *previewLayer;

@property (strong, nonatomic) AVCaptureDeviceInput *deviceInput;
@property (strong, nonatomic) AVCaptureStillImageOutput *stillCameraOutput;

@property (strong, nonatomic) dispatch_queue_t sessionQueue;

@property (strong, nonatomic) UITapGestureRecognizer *tapGesture;
@property (strong, nonatomic) UIPinchGestureRecognizer *pinchGesture;

@property (assign, nonatomic) CGFloat lastScale;
@property (assign, nonatomic) CGRect cropRect;

@end

@implementation ZWTCameraViewController

@synthesize session,backCamera,frontCamera,stillCameraOutput,deviceInput;
@synthesize previewLayer,sessionQueue;
@synthesize preView,upperView,lowerView,viewCrop;
@synthesize tapGesture,pinchGesture;
@synthesize btnCapture,btnRecentImage,btnCameraChange,btnFlashLight;

@synthesize lastScale,cropRect,recentImage,scrvCamera,viewCropLayer;

#pragma mark - UIViewController Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareView];
    [self prepareSession];
    [self addTapGesture];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (session)
    {
        [session startRunning];
    }
}

#pragma mark - UIView Methods
- (void)prepareView
{
    btnCapture.layer.cornerRadius  = CGRectGetHeight(btnCapture.frame)/2;
    btnCapture.layer.borderColor   = [UIColor lightGrayColor].CGColor;
    btnCapture.layer.borderWidth   = 3.0;
    btnCapture.layer.masksToBounds = YES;
    
    btnRecentImage.layer.cornerRadius = 3;
    btnRecentImage.clipsToBounds = YES;
    
    viewCrop.frame =  CGRectMake(0,
                                 CGRectGetMaxY(upperView.frame),
                                 CGRectGetWidth(viewCrop.frame),
                                 CGRectGetMinY(lowerView.frame)-CGRectGetMaxY(upperView.frame));

    scrvCamera.delegate         = self;
    scrvCamera.contentInset     = UIEdgeInsetsMake(0, 0, 0, 0);
    scrvCamera.minimumZoomScale = 1.0;
    scrvCamera.maximumZoomScale = 2.0;
    
    btnRecentImage.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    //[btnRecentImage setBackgroundImage:recentImage forState:UIControlStateNormal];
    
    [self setFrames];
}

- (AVCaptureVideoOrientation) videoOrientationFromCurrentDeviceOrientation:(UIInterfaceOrientation)orientation {
    switch (orientation) {
        case UIInterfaceOrientationPortrait: {
            return AVCaptureVideoOrientationPortrait;
        }
        case UIInterfaceOrientationLandscapeLeft: {
            return AVCaptureVideoOrientationLandscapeLeft;
        }
        case UIInterfaceOrientationLandscapeRight: {
            return AVCaptureVideoOrientationLandscapeRight;
        }
        case UIInterfaceOrientationPortraitUpsideDown: {
            return AVCaptureVideoOrientationPortraitUpsideDown;
        }
    }
    
    return 0;
}
- (void)setFrames
{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    CGFloat width = MIN(CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    
    previewLayer.frame = preView.frame;
    
    viewCropLayer.frame = self.view.bounds;
    
    CGRect frameViewTop;
    CGRect frameViewBottom;
    CGRect frameViewCrop;
    
    CGPoint centerBtnRecentImage;
    CGPoint centerBtnFlashLight;
    CGPoint centerBtnCameraChange;
    
    frameViewCrop.origin.x    = 0;
    frameViewCrop.origin.y    = 0;
    frameViewCrop.size.width  = width;
    frameViewCrop.size.height = width;
    
    viewCrop.frame  = frameViewCrop;
    viewCrop.center = CGPointMake(CGRectGetWidth(viewCropLayer.frame) / 2.0, CGRectGetHeight(viewCropLayer.frame) / 2.0);
    
    frameViewCrop = viewCrop.frame;
    
    if(orientation == UIInterfaceOrientationPortrait ||
       orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        frameViewTop.origin.x    = 0;
        frameViewTop.origin.y    = 0;
        frameViewTop.size.width  = CGRectGetWidth(frameViewCrop);
        frameViewTop.size.height = CGRectGetMinY(frameViewCrop);
        
        frameViewBottom.origin.x    = 0;
        frameViewBottom.origin.y    = CGRectGetMaxY(frameViewCrop);
        frameViewBottom.size.width  = CGRectGetWidth(frameViewCrop);
        frameViewBottom.size.height = CGRectGetHeight(frameViewTop);
        
        centerBtnCameraChange.x = CGRectGetWidth(frameViewBottom) * (12.5 / 100);
        centerBtnCameraChange.y = CGRectGetHeight(frameViewBottom) / 2.0;
        
        centerBtnFlashLight.x = CGRectGetWidth(frameViewBottom) * (30.5 / 100);
        centerBtnFlashLight.y = CGRectGetHeight(frameViewBottom) / 2.0;
    
        centerBtnRecentImage.x = CGRectGetWidth(frameViewBottom) * (80.0 / 100);
        centerBtnRecentImage.y = CGRectGetHeight(frameViewBottom) / 2.0;
    }
    else if (orientation == UIInterfaceOrientationLandscapeLeft ||
             orientation == UIInterfaceOrientationLandscapeRight)
    {
        frameViewCrop.size.width  = width;
        frameViewCrop.size.height = width;

        frameViewTop.origin.x    = 0;
        frameViewTop.origin.y    = 0;
        frameViewTop.size.width  = CGRectGetMinX(frameViewCrop);
        frameViewTop.size.height = CGRectGetHeight(frameViewCrop);
        
        frameViewBottom.origin.x    = CGRectGetMaxX(frameViewCrop);
        frameViewBottom.origin.y    = 0;
        frameViewBottom.size.width  = CGRectGetWidth(frameViewTop);
        frameViewBottom.size.height = CGRectGetHeight(frameViewTop);
        
        centerBtnRecentImage.x = CGRectGetWidth(frameViewBottom) / 2.0;
        centerBtnRecentImage.y = CGRectGetHeight(frameViewBottom) * (20.0 / 100);
        
        centerBtnFlashLight.x = CGRectGetWidth(frameViewBottom) / 2.0;
        centerBtnFlashLight.y = CGRectGetHeight(frameViewBottom) * (68.5 / 100);
        
        centerBtnCameraChange.x = CGRectGetWidth(frameViewBottom) / 2.0;
        centerBtnCameraChange.y = CGRectGetHeight(frameViewBottom) * (87.5 / 100);
    }
    
    upperView.frame = frameViewTop;
    lowerView.frame = frameViewBottom;
    
    btnCapture.center = CGPointMake(CGRectGetWidth(lowerView.frame) / 2.0, CGRectGetHeight(lowerView.frame) / 2.0);
    
    btnRecentImage.center  = centerBtnRecentImage;
    btnFlashLight.center   = centerBtnFlashLight;
    btnCameraChange.center = centerBtnCameraChange;
    
    viewCropLayer.transparentArea = viewCrop.frame;
    
    AVCaptureConnection *previewLayerConnection = self.previewLayer.connection;
    
    if ([previewLayerConnection isVideoOrientationSupported])
    {
        [previewLayerConnection setVideoOrientation:[self videoOrientationFromCurrentDeviceOrientation:orientation]];
    }
}

#pragma mark - CutomCamera Methods
- (void)prepareSession
{
    session = [[AVCaptureSession alloc]init];
    
    NSArray *availableDevice = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    
    for (AVCaptureDevice *device in availableDevice)
    {
        if (device.position == AVCaptureDevicePositionBack)
        {
            backCamera = device;
        }
        else if (device.position == AVCaptureDevicePositionFront)
        {
            frontCamera = device;
        }
    }
    
    NSError *error;
    
    deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:backCamera error:&error];
    
    if ([session canAddInput:deviceInput])
    {
        [session addInput:deviceInput];
    }
    
    if ([UIScreen mainScreen].bounds.size.height != 480)
    {
        preView.frame = self.view.frame;
    }
    
    previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
    previewLayer.frame  = preView.frame;
    
    [preView.layer addSublayer:previewLayer];
    
    sessionQueue = dispatch_queue_create("session queue", DISPATCH_QUEUE_SERIAL);
    
    dispatch_async(sessionQueue, ^
    {
        [session startRunning];
        
        stillCameraOutput = [[AVCaptureStillImageOutput alloc]init];
        
        if ([session canAddOutput:stillCameraOutput])
        {
            [session addOutput:stillCameraOutput];
        }
    });
}

#pragma mark - Gesture Methods
- (void)removeGesture
{
    [preView removeGestureRecognizer:tapGesture];
    [preView removeGestureRecognizer:pinchGesture];
}

- (void)addTapGesture
{
    tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapDone:)];
    
    tapGesture.numberOfTapsRequired = 1;
    
    [preView addGestureRecognizer:tapGesture];
}

- (void)addPinchGesture
{
    pinchGesture = [[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(pinchDone:)];
    
    preView.userInteractionEnabled = YES;
    
    [preView addGestureRecognizer:pinchGesture];
}

#pragma mark - Events Methods
- (IBAction)tapDone:(UITapGestureRecognizer *)gestureRecognizer
{
    NSError *error;
    
    if ([backCamera lockForConfiguration:&error])
    {
        CGPoint pointInPreView = [gestureRecognizer locationInView:gestureRecognizer.view];
        CGPoint pointIncamera  = [previewLayer captureDevicePointOfInterestForPoint:pointInPreView];
        
        backCamera.focusPointOfInterest = pointIncamera;
        backCamera.focusMode = AVCaptureFocusModeAutoFocus;
    }
}

- (void)pinchDone:(UIPinchGestureRecognizer *)gestureRecognizer
{
    if([gestureRecognizer state] == UIGestureRecognizerStateBegan)
    {
        // Reset the last scale, necessary if there are multiple objects with different scales
        lastScale = [gestureRecognizer scale];
    }
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan ||
        [gestureRecognizer state] == UIGestureRecognizerStateChanged)
    {
        
        CGFloat currentScale = [[[gestureRecognizer view].layer valueForKeyPath:@"transform.scale"] floatValue];
        
        // Constants to adjust the max/min values of zoom
        const CGFloat kMaxScale = 2.0;
        const CGFloat kMinScale = 1.0;
        
        CGFloat newScale = 1 -  (lastScale - [gestureRecognizer scale]);
        
        newScale = MIN(newScale, kMaxScale / currentScale);
        newScale = MAX(newScale, kMinScale / currentScale);
        
        CGAffineTransform transform = CGAffineTransformScale([[gestureRecognizer view] transform], newScale, newScale);
        
        [gestureRecognizer view].transform = transform;
        
        lastScale = [gestureRecognizer scale];  // Store the previous scale factor for the next pinch gesture call
    }
}

- (IBAction)btnCloseTap:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnCameraChangeTap:(id)sender
{
    NSError *error;
    
    [session inputs];
    
    AVCaptureDeviceInput *cameraPosition = [[session inputs] objectAtIndex:0];
    
    [session beginConfiguration];
    [session removeInput:deviceInput];
    
    switch (cameraPosition.device.position)
    {
        case AVCaptureDevicePositionBack:
            deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:frontCamera error:&error];
            break;
            
        case AVCaptureDevicePositionFront:
            deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:backCamera error:&error];
            break;
            
        case AVCaptureDevicePositionUnspecified:
            deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:frontCamera error:&error];
            break;
    }
    
    [session addInput:deviceInput];
    
    [session commitConfiguration];
}

- (IBAction)btnFlashLightTap:(UIButton *)sender
{
    NSError *error;
    
    if ([backCamera lockForConfiguration:&error])
    {
        if(sender.selected)
        {
            backCamera.flashMode = AVCaptureFlashModeOff;
            sender.selected = NO;
        }
        else if (!sender.selected)
        {
            backCamera.flashMode = AVCaptureFlashModeOn;
            sender.selected = YES;
        }
    }
}

- (IBAction)btnCameraImageCaptureTap:(id)sender
{
    dispatch_async(sessionQueue, ^
    {
        AVCaptureConnection *connection = [stillCameraOutput connectionWithMediaType:AVMediaTypeVideo];
        
        connection.videoOrientation = (AVCaptureVideoOrientation)[UIDevice currentDevice].orientation;
        
        [stillCameraOutput captureStillImageAsynchronouslyFromConnection:connection
                                                       completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error)
         {
             if (!error)
             {
                 [session stopRunning];
                 
                 UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
                 
                 NSData *imgData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                 
                 UIImage *imgCaptured = [UIImage imageWithData:imgData];
                 
                 CGRect cropRects = [viewCropLayer convertRect:viewCrop.frame toView:preView];
                 
                 CGFloat factor;
                 
                 if(orientation == UIInterfaceOrientationPortrait ||
                    orientation == UIInterfaceOrientationPortraitUpsideDown||
                    orientation == UIInterfaceOrientationUnknown)
                 {
                     factor = (imgCaptured.size.width * imgCaptured.scale) / CGRectGetWidth(self.view.frame);
                 }
                 else
                 {
                     factor = (imgCaptured.size.height * imgCaptured.scale) / CGRectGetWidth(viewCrop.frame);
                 }
                 
                 cropRects.origin.x *= factor;
                 cropRects.origin.y *= factor;
                 
                 cropRects.size.width  *= factor;
                 cropRects.size.height *= factor;
  
                 UIImage *imgvCroped = [imgCaptured cropedImagewithCropRect:cropRects];
                 
                 btnFlashLight.selected = YES;
                 
                 [self btnFlashLightTap:btnFlashLight];
                 
                 dispatch_async(dispatch_get_main_queue(), ^
                 {
                     [self.pickerDelegate imagePickerController:(ZWTImagePickerController *)self.navigationController
                                          didFinishPickingMedia:@[imgvCroped]];
                 });
             }
             else
             {
                 NSLog(@"Some error %@",error.description);
             }
         }];
    });
}

- (IBAction)btnRecentImageTap:(id)sender
{
    ZWTAlbumViewController *vcCollection = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowPhotoColletionViewControllerID"];
    
    PHCollection *collection = [[PHCollection alloc] init];// PHCGlobalObject.cameraRolls[0];
    
    if ([collection isKindOfClass:[PHAssetCollection class]])
    {
        PHAssetCollection *assetCollection = (PHAssetCollection *)collection;
        
        PHFetchOptions *options = [[PHFetchOptions alloc] init];
        
        options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
        options.predicate       = [NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeImage];
        
        PHFetchResult *assetsFetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:options];
        
        vcCollection.assetsFetchResults = assetsFetchResult;
        vcCollection.assetCollection    = assetCollection;
        
        [self.navigationController pushViewController:vcCollection animated:YES];
    }
}

#pragma mark - UIScrollViewDelegate Methods
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return preView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    [self centerScrollViewContents];
}

#pragma mark - Helper Methods
+ (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

- (void)centerScrollViewContents
{
    CGSize boundsSize    = self.scrvCamera.bounds.size;
    CGRect contentsFrame = preView.frame;
    
    if (contentsFrame.size.width < boundsSize.width)
    {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    }
    else
    {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height)
    {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    }
    else
    {
        contentsFrame.origin.y = 0.0f;
    }
    
    preView.frame = contentsFrame;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
    {
        [self setFrames];
    }
                                completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
    {
         
    }];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

@end