//
//  UIImage+FLZPrepareFlag.h
//  Flagz
//
//  Created by Vivek on 28/08/14.
//  Copyright (c) 2014 zwt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ZWTImage)

- (UIImage *)cropedImagewithCropRect:(CGRect)cropRect;

@end