//
//  TouchTansperentView.h
//  ImageDemo
//
//  Created by Chintan Dave on 27/04/15.
//  Copyright (c) 2015 Chintan Dave. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZWTTouchTansperentView : UIView

@property (nonatomic) CGRect transparentArea;

@end