//
//  UIImage+FLZPrepareFlag.m
//  Flagz
//
//  Created by Vivek on 28/08/14.
//  Copyright (c) 2014 zwt. All rights reserved.
//

#import "UIImage+ZWTImage.h"

@implementation UIImage (ZWTImage)

/**
 *  This method will crop image with give rectangle
 *
 *  @param image    Object of UIImage Which you want to crop
 *  @param cropRect Object of CGRect which you want to crop from imaeg.
 If you are selection image from UIImagePickerController you can get user selected crop Rectangle by
 [[info objectForKey:UIImagePickerControllerCropRect] CGRectValue]
 *
 *  @return Object of croped UIImage with giver CGRect.
 */
- (UIImage *)cropedImagewithCropRect:(CGRect)cropRect
{
	CGSize size = self.size;
    
    size = CGSizeMake(size.width * self.scale, size.height * self.scale);

	
	// create a graphics context of the correct size
	UIGraphicsBeginImageContext(cropRect.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	// correct for image orientation
	UIImageOrientation orientation = [self imageOrientation];
	
	if(orientation == UIImageOrientationUp)
	{
		CGContextTranslateCTM(context, 0, size.height);
		
		CGContextScaleCTM(context, 1, -1);
        
		cropRect = CGRectMake(cropRect.origin.x,
							  -cropRect.origin.y,
							  cropRect.size.width,
							  cropRect.size.height);
	}
	else if(orientation == UIImageOrientationRight)
	{
		CGContextScaleCTM(context, 1.0, -1.0);

		CGContextRotateCTM(context, -M_PI/2);
		
		size = CGSizeMake(size.height, size.width);
		
		cropRect = CGRectMake(cropRect.origin.y,
							  cropRect.origin.x,
							  cropRect.size.height,
							  cropRect.size.width);
	}
	else if(orientation == UIImageOrientationDown)
	{
		CGContextTranslateCTM(context, size.width, 0);
		
		CGContextScaleCTM(context, -1, 1);
		
		cropRect = CGRectMake(-cropRect.origin.x,
							  cropRect.origin.y,
							  cropRect.size.width,
							  cropRect.size.height);
	}
	
	// draw the image in the correct place
	CGContextTranslateCTM(context, -cropRect.origin.x, -cropRect.origin.y);
	
	CGContextDrawImage(context, CGRectMake(0,0, size.width, size.height), self.CGImage);
	
	// and pull out the cropped image
	UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
	
	UIGraphicsEndImageContext();
	
	return croppedImage;
}

@end