//
//  TouchTansperentView.m
//  ImageDemo
//
//  Created by Chintan Dave on 27/04/15.
//  Copyright (c) 2015 Chintan Dave. All rights reserved.
//

#import "ZWTTouchTansperentView.h"

@implementation ZWTTouchTansperentView

@synthesize transparentArea;

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    if(CGRectIsEmpty(transparentArea))
    {
        return NO;
    }
    else if(CGRectContainsPoint(transparentArea, point))
    {
        return NO;
    }

    return YES;
}

@end