//
//  PhotoLibraryViewController.m
//  Photocopy
//
//  Created by Vivek on 21/04/15.
//  Copyright (c) 2015 Chintan Dave. All rights reserved.
//

#import "ZWTPhotoLibraryViewController.h"
#import "ZWTAlbumViewController.h"
#import "ZWTAlbumCell.h"

static NSString *const AllPhotosReuseIdentifier      = @"AllPhotosCell";
static NSString *const CollectionCellReuseIdentifier = @"AlbumCell";

static NSString *const AllPhotosSegue  = @"showAllPhotos";
static NSString *const CollectionSegue = @"showAlbum";

@import Photos;

@interface ZWTPhotoLibraryViewController()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblvPhotos;

@property (strong, nonatomic) NSMutableArray *collectionsFetchResults;
@property (strong, nonatomic) NSArray *collectionsLocalizedTitles;

@property (strong, nonatomic) PHCachingImageManager *imageManager;

@end

@implementation ZWTPhotoLibraryViewController

@synthesize tblvPhotos, collectionsFetchResults, imageManager;

#pragma mark - UIViewController Methods
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self prepareView];
    
    [self prepareLibraryList];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:AllPhotosSegue])
    {
        ZWTAlbumViewController *vcCollection = segue.destinationViewController;
        
        PHFetchOptions *options = [[PHFetchOptions alloc] init];
        
        options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
        options.predicate       = [NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeImage];
        
        vcCollection.assetsFetchResults = [PHAsset fetchAssetsWithOptions:options];
    }
    else if ([segue.identifier isEqualToString:CollectionSegue])
    {
        ZWTAlbumViewController *vcCollection = segue.destinationViewController;
        
        NSIndexPath *indexPath        = [tblvPhotos indexPathForCell:sender];
        PHAssetCollection *collection = self.collectionsFetchResults[indexPath.row - 1];
    
        if ([collection isKindOfClass:[PHAssetCollection class]])
        {
            PHAssetCollection *assetCollection = (PHAssetCollection *)collection;
            
            PHFetchOptions *options = [[PHFetchOptions alloc] init];
            
            options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
            options.predicate       = [NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeImage];
            
            PHFetchResult *assetsFetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:options];
            
            vcCollection.assetsFetchResults = assetsFetchResult;
            vcCollection.assetCollection = assetCollection;
        }
    }
}

#pragma mark - View Methods
- (void)prepareView
{
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark - Helper Methods
- (void)prepareLibraryList
{
    tblvPhotos.tableFooterView = [[UIView alloc] init];
    
    imageManager = [[PHCachingImageManager alloc] init];
    
    collectionsFetchResults = [[NSMutableArray alloc]init];
    
    PHFetchResult *genericAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum
                                                                            subtype:PHAssetCollectionSubtypeSmartAlbumGeneric
                                                                            options:nil];
    [self addCoolectionToList:genericAlbums];
    
    PHFetchResult *panaromasAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum
                                                                              subtype:PHAssetCollectionSubtypeSmartAlbumPanoramas
                                                                              options:nil];
    [self addCoolectionToList:panaromasAlbums];
    
    PHFetchResult *favoriteAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum
                                                                             subtype:PHAssetCollectionSubtypeSmartAlbumFavorites
                                                                             options:nil];
    [self addCoolectionToList:favoriteAlbums];
    
    PHFetchResult *timeAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum
                                                                         subtype:PHAssetCollectionSubtypeSmartAlbumTimelapses
                                                                         options:nil];
    [self addCoolectionToList:timeAlbums];
    
    PHFetchResult *recentlyAddedAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum
                                                                                  subtype:PHAssetCollectionSubtypeSmartAlbumRecentlyAdded
                                                                                  options:nil];
    [self addCoolectionToList:recentlyAddedAlbums];
    
    PHFetchResult *burstAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum
                                                                          subtype:PHAssetCollectionSubtypeSmartAlbumBursts
                                                                          options:nil];
    [self addCoolectionToList:burstAlbums];
    
    PHFetchResult *userAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum
                                                                         subtype:PHAssetCollectionSubtypeSmartAlbumUserLibrary
                                                                         options:nil];
    [self addCoolectionToList:userAlbums];
    
    PHFetchResult *topLevelUserCollections = [PHCollectionList fetchTopLevelUserCollectionsWithOptions:nil];
    
    [self addCoolectionToList:topLevelUserCollections];
}

- (void)addCoolectionToList:(PHFetchResult *)fetchResult
{
    if (fetchResult.count > 0)
    {
        for (int i = 0; i < fetchResult.count; i++)
        {
            PHAssetCollection *collection = (PHAssetCollection *)fetchResult[i];
            
            PHFetchOptions *options = [[PHFetchOptions alloc] init];
            
            options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
            options.predicate       = [NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeImage];
            
            PHFetchResult *assetsFetchResult = [PHAsset fetchAssetsInAssetCollection:collection options:options];
            
            if (assetsFetchResult.count > 0)
            {
                [collectionsFetchResults addObject:collection];
            }
        }
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return collectionsFetchResults.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        ZWTAlbumCell *cell = [tableView dequeueReusableCellWithIdentifier:AllPhotosReuseIdentifier forIndexPath:indexPath];
        
        PHFetchOptions *options = [[PHFetchOptions alloc] init];
        
        options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
        options.predicate       = [NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeImage];
        
        PHFetchResult *assetsFetchResults = [PHAsset fetchAssetsWithOptions:options];

        PHAsset *asset = [assetsFetchResults firstObject];
        
        PHImageRequestOptions *option = [PHImageRequestOptions new];
        
        option.synchronous = YES;
        
        [imageManager requestImageForAsset:asset
                                targetSize:CGSizeMake(75 * [UIScreen mainScreen].scale, 75 * [UIScreen mainScreen].scale)
                               contentMode:PHImageContentModeAspectFill
                                   options:option
                             resultHandler:^(UIImage *result, NSDictionary *info)
        {
            cell.imgvThumb.image = result;
        }];
        
        cell.lblAlbumName.text = @"All Photos";
        cell.lblItemCount.text = [NSString stringWithFormat:@"%@", @(assetsFetchResults.count)];

        return cell;
    }
    else
    {
        ZWTAlbumCell *cell = [tableView dequeueReusableCellWithIdentifier:CollectionCellReuseIdentifier forIndexPath:indexPath];

        PHAssetCollection *collection = self.collectionsFetchResults[indexPath.row - 1];
        
        cell.lblAlbumName.text = collection.localizedTitle;
        
        PHAssetCollection *assetCollection = (PHAssetCollection *)collection;
        
        PHFetchOptions *options = [[PHFetchOptions alloc] init];
        
        options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
        options.predicate       = [NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeImage];
        
        PHFetchResult *assetsFetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:options];
        
        PHAsset *asset = [assetsFetchResult firstObject];
        
        PHImageRequestOptions *option = [PHImageRequestOptions new];
        
        option.synchronous = YES;
        
        [imageManager requestImageForAsset:asset
                                targetSize:CGSizeMake(75 * [UIScreen mainScreen].scale, 75 * [UIScreen mainScreen].scale)
                               contentMode:PHImageContentModeAspectFill
                                   options:option
                             resultHandler:^(UIImage *result, NSDictionary *info)
        {
            cell.imgvThumb.image = result;
        }];

        cell.lblItemCount.text = [NSString stringWithFormat:@"%@", @(assetsFetchResult.count)];
        
        return cell;
    }
}

#pragma mark - Events Methods
- (IBAction)btnBackTap:(id)sender
{
    [self.pickerDelegate imagePickerControllerDidCancel:(ZWTImagePickerController *)self.navigationController];
}

@end