//
//  ZWTAlbumCell.h
//  ZWTImagePickerController
//
//  Created by Chintan Dave on 13/10/15.
//  Copyright © 2015 Chintan Dave. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZWTAlbumCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgvThumb;
@property (weak, nonatomic) IBOutlet UILabel *lblAlbumName;
@property (weak, nonatomic) IBOutlet UILabel *lblItemCount;

@end