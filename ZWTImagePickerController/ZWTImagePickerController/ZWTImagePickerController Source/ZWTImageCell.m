//
//  PhotoCollectionViewCell.m
//  PhotocopyDemo
//
//  Created by Vivek on 17/04/15.
//  Copyright (c) 2015 Adnan. All rights reserved.
//

#import "ZWTImageCell.h"

@interface ZWTImageCell ()

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation ZWTImageCell

@synthesize thumbnailImage,imageView;

- (void)setThumbnailImage:(UIImage *)thumbnailImageDetail
{
    thumbnailImage = thumbnailImageDetail;
    
    imageView.layer.cornerRadius = 3.0;
    imageView.clipsToBounds = YES;
    
    imageView.image = thumbnailImage;
}

@end
