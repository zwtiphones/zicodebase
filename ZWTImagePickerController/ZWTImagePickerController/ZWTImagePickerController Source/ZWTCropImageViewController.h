//
//  CropViewController.h
//  salvos_mockup
//
//  Created by Kapil on 27/04/15.
//  Copyright (c) 2015 Kapil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZWTImagePickerController.h"

@interface ZWTCropImageViewController : UIViewController

@property (strong, nonatomic) UIImage *_Nonnull imgPhoto;

@property (nullable, nonatomic, weak) id <ZWTImagePickerControllerDelegate> pickerDelegate;

@property (strong, nonatomic) PHFetchResult *_Nullable cameraRolls;

@end