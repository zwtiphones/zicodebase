//
//  ZWTAlbumCell.m
//  ZWTImagePickerController
//
//  Created by Chintan Dave on 13/10/15.
//  Copyright © 2015 Chintan Dave. All rights reserved.
//

#import "ZWTAlbumCell.h"

@interface ZWTAlbumCell ()

@end

@implementation ZWTAlbumCell

@synthesize imgvThumb;

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    imgvThumb.layer.cornerRadius = 10.0;
    imgvThumb.layer.borderColor  = [UIColor grayColor].CGColor;
    imgvThumb.layer.borderWidth  = 1.0;
}

@end
