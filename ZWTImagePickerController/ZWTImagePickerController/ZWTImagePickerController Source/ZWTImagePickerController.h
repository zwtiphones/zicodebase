//
//  PhotoPickerNavigationController.h
//  Photocopy
//
//  Created by Vivek on 21/04/15.
//  Copyright (c) 2015 Chintan Dave. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Photos;

@class ZWTImagePickerController;

@protocol ZWTImagePickerControllerDelegate <NSObject>

- (void)imagePickerController:(ZWTImagePickerController *_Nonnull)picker didFinishPickingMedia:(NSArray *_Nonnull)media;
- (void)imagePickerControllerDidCancel:(ZWTImagePickerController *_Nonnull)picker;

@optional
- (void)imagePickerController:(ZWTImagePickerController *_Nonnull)picker didFailWIthError:(NSString *_Nonnull)errorMessage;

@end

@interface ZWTImagePickerController : UINavigationController

@property (nullable, nonatomic, weak) id <ZWTImagePickerControllerDelegate, UINavigationControllerDelegate> delegate;

@property (nonatomic) UIImagePickerControllerSourceType sourceType;

@property (nonatomic, copy) NSArray<NSString *> *_Nullable mediaTypes;

@property (nonatomic) BOOL allowsEditing;

@end