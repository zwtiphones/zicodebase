//
//  ShowPhotoColletionViewController.m
//  Photocopy
//
//  Created by Vivek on 21/04/15.
//  Copyright (c) 2015 Chintan Dave. All rights reserved.
//

#import "ZWTAlbumViewController.h"
#import "ZWTPhotoLibraryViewController.h"
#import "ZWTImagePickerController.h"
#import "ZWTImageCell.h"
#import "ZWTCropImageViewController.h"

static NSString *const reuseIdentifier = @"ImageCell";

static CGSize AssetGridThumbnailSize;

@implementation NSIndexSet (Convenience)

- (NSArray *)aapl_indexPathsFromIndexesWithSection:(NSUInteger)section
{
    NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:self.count];
    
    [self enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop)
    {
        [indexPaths addObject:[NSIndexPath indexPathForItem:idx inSection:section]];
    }];
    
    return indexPaths;
}

@end

@implementation UICollectionView (Convenience)

- (NSArray *)aapl_indexPathsForElementsInRect:(CGRect)rect
{
    NSArray *allLayoutAttributes = [self.collectionViewLayout layoutAttributesForElementsInRect:rect];

    if (allLayoutAttributes.count == 0)
    {
        return nil;
    }
    
    NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:allLayoutAttributes.count];
    
    for (UICollectionViewLayoutAttributes *layoutAttributes in allLayoutAttributes)
    {
        NSIndexPath *indexPath = layoutAttributes.indexPath;
    
        [indexPaths addObject:indexPath];
    }
    
    return indexPaths;
}

@end

@interface ZWTAlbumViewController () <PHPhotoLibraryChangeObserver>

@property (strong, nonatomic) PHCachingImageManager *imageManager;

@property (weak, nonatomic) IBOutlet UICollectionView *clvPhotos;

@property CGRect previousPreheatRect;

@end

@implementation ZWTAlbumViewController

@synthesize previousPreheatRect,imageManager,assetsFetchResults,assetCollection,clvPhotos;

#pragma mark - UIViewController Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(assetCollection)
    {
        self.navigationItem.title = assetCollection.localizedTitle;
    }
    else
    {
        self.navigationItem.title = @"All Photos";
    }
}

- (void)awakeFromNib
{
    self.imageManager = [[PHCachingImageManager alloc] init];
    
    [self resetCachedAssets];
    
    [[PHPhotoLibrary sharedPhotoLibrary] registerChangeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    CGSize cellSize = ((UICollectionViewFlowLayout *)clvPhotos.collectionViewLayout).itemSize;
    
    AssetGridThumbnailSize = CGSizeMake(cellSize.width, cellSize.height);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self updateCachedAssets];
}

#pragma mark - PHPhotoLibraryChangeObserver
- (void)photoLibraryDidChange:(PHChange *)changeInstance
{
    // Call might come on any background queue. Re-dispatch to the main queue to handle it.
    dispatch_async(dispatch_get_main_queue(), ^
    {
        // check if there are changes to the assets (insertions, deletions, updates)
        PHFetchResultChangeDetails *collectionChanges = [changeInstance changeDetailsForFetchResult:self.assetsFetchResults];
        if (collectionChanges)
        {
            // get the new fetch result
            self.assetsFetchResults = [collectionChanges fetchResultAfterChanges];
            
            UICollectionView *collectionView = clvPhotos;
            
            if (![collectionChanges hasIncrementalChanges] || [collectionChanges hasMoves])
            {
                // we need to reload all if the incremental diffs are not available
                [collectionView reloadData];
            }
            else
            {
                // if we have incremental diffs, tell the collection view to animate insertions and deletions
                [collectionView performBatchUpdates:^
                {
                    NSIndexSet *removedIndexes = [collectionChanges removedIndexes];
                    
                    if ([removedIndexes count])
                    {
                        [collectionView deleteItemsAtIndexPaths:[removedIndexes aapl_indexPathsFromIndexesWithSection:0]];
                    }
                    
                    NSIndexSet *insertedIndexes = [collectionChanges insertedIndexes];
                    
                    if ([insertedIndexes count])
                    {
                        [collectionView insertItemsAtIndexPaths:[insertedIndexes aapl_indexPathsFromIndexesWithSection:0]];
                    }
                    
                    NSIndexSet *changedIndexes = [collectionChanges changedIndexes];
                    
                    if ([changedIndexes count])
                    {
                        [collectionView reloadItemsAtIndexPaths:[changedIndexes aapl_indexPathsFromIndexesWithSection:0]];
                    }
                } completion:nil];
            }
            
            [self resetCachedAssets];
        }
    });
}

#pragma mark - UICollectionViewDataSource Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (assetsFetchResults.count == 0)
    {
        [self.navigationController popViewControllerAnimated:YES];
        
        return 0;
    }
    else
    {
        NSInteger count = assetsFetchResults.count;
        
        return count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZWTImageCell *cell = (ZWTImageCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Increment the cell's tag
    NSInteger currentTag = cell.tag + 1;
    
    cell.tag = currentTag;
    
    PHAsset *asset = assetsFetchResults[indexPath.item];
    
    PHImageRequestOptions *option = [PHImageRequestOptions new];
    
    option.synchronous = YES;
   
    [self.imageManager requestImageForAsset:asset
                                 targetSize:AssetGridThumbnailSize
                                contentMode:PHImageContentModeAspectFill
                                    options:option
                              resultHandler:^(UIImage *result, NSDictionary *info)
    {
        // Only update the thumbnail if the cell tag hasn't changed. Otherwise, the cell has been re-used.
        if (cell.tag == currentTag)
        {
            dispatch_async(dispatch_get_main_queue(), ^
            {
                cell.thumbnailImage = result;
            });
        }
    }];
  
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PHAsset *asset = [self.assetsFetchResults objectAtIndex:indexPath.row];
    
    [[PHImageManager defaultManager] requestImageDataForAsset:asset
                                                      options:nil
                                                resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info)
    {
        UIImage *imgSelected = [UIImage imageWithData:imageData];
        
        if (orientation == UIImageOrientationRight || orientation == UIImageOrientationLeft || orientation == UIImageOrientationDown)
        {
            UIGraphicsBeginImageContextWithOptions(imgSelected.size, NO, imgSelected.scale);
            
            [imgSelected drawInRect:(CGRect){0, 0, imgSelected.size}];
            
            imgSelected = UIGraphicsGetImageFromCurrentImageContext();
            
            UIGraphicsEndImageContext();
        }

        ZWTCropImageViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"ZWTCropImageViewController"];
        
        cvc.imgPhoto = imgSelected;
        
        [self.navigationController pushViewController:cvc animated:YES];
        
//        [self.pickerDelegate imagePickerController:(ZWTImagePickerController *)self.navigationController
//                             didFinishPickingMedia:@[imgSelected]];
    }];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat parameter = MIN(CGRectGetWidth(collectionView.frame), CGRectGetHeight(collectionView.frame));
    
    return CGSizeMake(parameter/3 - 2, parameter/3 - 2);
}

#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updateCachedAssets];
}

#pragma mark - Asset Caching
- (void)resetCachedAssets
{
    [self.imageManager stopCachingImagesForAllAssets];
    
    self.previousPreheatRect = CGRectZero;
}

- (void)updateCachedAssets
{
    BOOL isViewVisible = [self isViewLoaded] && [[self view] window] != nil;
    if (!isViewVisible) { return; }
    
    // The preheat window is twice the height of the visible rect
    CGRect preheatRect = clvPhotos.bounds;
    preheatRect = CGRectInset(preheatRect, 0.0f, -0.5f * CGRectGetHeight(preheatRect));
    
    // If scrolled by a "reasonable" amount...
    CGFloat delta = ABS(CGRectGetMidY(preheatRect) - CGRectGetMidY(self.previousPreheatRect));
    if (delta > CGRectGetHeight(clvPhotos.bounds) / 3.0f)
    {
        // Compute the assets to start caching and to stop caching.
        NSMutableArray *addedIndexPaths = [NSMutableArray array];
        NSMutableArray *removedIndexPaths = [NSMutableArray array];
        
        [self computeDifferenceBetweenRect:self.previousPreheatRect andRect:preheatRect removedHandler:^(CGRect removedRect)
        {
            NSArray *indexPaths = [clvPhotos aapl_indexPathsForElementsInRect:removedRect];
        
            [removedIndexPaths addObjectsFromArray:indexPaths];
        }
                            addedHandler:^(CGRect addedRect)
        {
            NSArray *indexPaths = [clvPhotos aapl_indexPathsForElementsInRect:addedRect];
            
            [addedIndexPaths addObjectsFromArray:indexPaths];
        }];
        
        NSArray *assetsToStartCaching = [self assetsAtIndexPaths:addedIndexPaths];
        NSArray *assetsToStopCaching  = [self assetsAtIndexPaths:removedIndexPaths];
        
        [self.imageManager startCachingImagesForAssets:assetsToStartCaching
                                            targetSize:AssetGridThumbnailSize
                                           contentMode:PHImageContentModeAspectFill
                                               options:nil];
        
        [self.imageManager stopCachingImagesForAssets:assetsToStopCaching
                                           targetSize:AssetGridThumbnailSize
                                          contentMode:PHImageContentModeAspectFill
                                              options:nil];
        
        self.previousPreheatRect = preheatRect;
    }
}

- (void)computeDifferenceBetweenRect:(CGRect)oldRect
                             andRect:(CGRect)newRect
                      removedHandler:(void (^)(CGRect removedRect))removedHandler
                        addedHandler:(void (^)(CGRect addedRect))addedHandler
{
    if (CGRectIntersectsRect(newRect, oldRect))
    {
        CGFloat oldMaxY = CGRectGetMaxY(oldRect);
        CGFloat oldMinY = CGRectGetMinY(oldRect);
        CGFloat newMaxY = CGRectGetMaxY(newRect);
        CGFloat newMinY = CGRectGetMinY(newRect);
        
        if (newMaxY > oldMaxY)
        {
            CGRect rectToAdd = CGRectMake(newRect.origin.x, oldMaxY, newRect.size.width, (newMaxY - oldMaxY));
            
            addedHandler(rectToAdd);
        }
        
        if (oldMinY > newMinY)
        {
            CGRect rectToAdd = CGRectMake(newRect.origin.x, newMinY, newRect.size.width, (oldMinY - newMinY));
            
            addedHandler(rectToAdd);
        }
        
        if (newMaxY < oldMaxY)
        {
            CGRect rectToRemove = CGRectMake(newRect.origin.x, newMaxY, newRect.size.width, (oldMaxY - newMaxY));
            
            removedHandler(rectToRemove);
        }
        
        if (oldMinY < newMinY)
        {
            CGRect rectToRemove = CGRectMake(newRect.origin.x, oldMinY, newRect.size.width, (newMinY - oldMinY));
            
            removedHandler(rectToRemove);
        }
    }
    else
    {
        addedHandler(newRect);
        removedHandler(oldRect);
    }
}

- (NSArray *)assetsAtIndexPaths:(NSArray *)indexPaths
{
    if (indexPaths.count == 0)
    {
        return nil;
    }
    
    NSMutableArray *assets = [NSMutableArray arrayWithCapacity:indexPaths.count];
    
    for (NSIndexPath *indexPath in indexPaths)
    {
        PHAsset *asset = assetsFetchResults[indexPath.item];
    
        [assets addObject:asset];
    }
    
    return assets;
}

- (IBAction)btnBackTap:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
