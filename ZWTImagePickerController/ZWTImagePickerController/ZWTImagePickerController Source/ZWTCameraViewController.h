//
//  CustomCameraViewController.h
//  Photocopy
//
//  Created by Vivek on 21/04/15.
//  Copyright (c) 2015 Chintan Dave. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZWTImagePickerController.h"

@interface ZWTCameraViewController : UIViewController

@property (strong, nonatomic) UIImage *_Nullable recentImage;

@property (nullable, nonatomic, weak) id <ZWTImagePickerControllerDelegate> pickerDelegate;

@property (strong, nonatomic) PHFetchResult *_Nullable cameraRolls;

@end