//
//  PhotoCollectionViewCell.h
//  PhotocopyDemo
//
//  Created by Vivek on 17/04/15.
//  Copyright (c) 2015 Adnan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZWTImageCell : UICollectionViewCell

@property (nonatomic, strong) UIImage *thumbnailImage;

@end