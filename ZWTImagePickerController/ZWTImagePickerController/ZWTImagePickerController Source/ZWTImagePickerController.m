//
//  PhotoPickerNavigationController.m
//  Photocopy
//
//  Created by Vivek on 21/04/15.
//  Copyright (c) 2015 Chintan Dave. All rights reserved.
//

#import "ZWTImagePickerController.h"
#import "ZWTPhotoLibraryViewController.h"
#import "ZWTCameraViewController.h"

@interface ZWTImagePickerController ()<UINavigationControllerDelegate>

@property (nullable, nonatomic, weak) id <ZWTImagePickerControllerDelegate> pickerDelegate;
@property (nonnull, nonatomic, strong) UIStoryboard *imagePickerStoryboard;

@property (strong, nonatomic) PHFetchResult *_Nullable cameraRolls;

@end

@implementation ZWTImagePickerController

@dynamic delegate;
@synthesize sourceType, imagePickerStoryboard, cameraRolls;

#pragma mark - UINavigationController Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareCameraRolls];
    [self setRootViewController];
}

#pragma mark - Helper Methods
- (UIStoryboard *)imagePickerStoryboard
{
    if(imagePickerStoryboard)
    {
        return imagePickerStoryboard;
    }
    
    imagePickerStoryboard = [UIStoryboard storyboardWithName:@"ZWTImagePicker" bundle:nil];
    
    return imagePickerStoryboard;
}

- (void)setDelegate:(id<ZWTImagePickerControllerDelegate, UINavigationControllerDelegate>)delegateObject
{
    [super setDelegate:self];
    
    self.pickerDelegate = delegateObject;
}

- (void)setRootViewController
{
    switch (sourceType)
    {
        case UIImagePickerControllerSourceTypePhotoLibrary:
        case UIImagePickerControllerSourceTypeSavedPhotosAlbum:
        {
            ZWTPhotoLibraryViewController *pvc = [self.imagePickerStoryboard instantiateViewControllerWithIdentifier:@"PhotoLibraryViewControllerID"];
            
            self.viewControllers = @[pvc];
            
            break;
        }
        case UIImagePickerControllerSourceTypeCamera:
        {
            ZWTCameraViewController *ccvc = [self.imagePickerStoryboard instantiateViewControllerWithIdentifier:@"CustomCameraViewControllerID"];
            
            self.viewControllers = @[ccvc];
            
            break;
        }
    }
}

- (void)prepareCameraRolls
{
    __weak ZWTImagePickerController *weakSelf = self;
    
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status)
    {
        if(status == PHAuthorizationStatusAuthorized)
        {
            weakSelf.cameraRolls = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum
                                                                        subtype:PHAssetCollectionSubtypeSmartAlbumUserLibrary
                                                                        options:nil];
        }
        else
        {
            [self.pickerDelegate imagePickerController:self didFailWIthError:@"User Not Authorised."];
        }
    }];
}

#pragma mark - UINavigationControllerDelegate Methods
- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated
{
    [viewController setValue:self.pickerDelegate forKey:@"pickerDelegate"];
    [viewController setValue:self.cameraRolls forKey:@"cameraRolls"];
    
    if(![viewController isKindOfClass:[ZWTCameraViewController class]])
    {
        [[viewController navigationController] setNavigationBarHidden:NO animated:NO];
    }
}

@end