//
//  main.m
//  ZWTImagePickerController
//
//  Created by Chintan Dave on 13/10/15.
//  Copyright © 2015 Chintan Dave. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
