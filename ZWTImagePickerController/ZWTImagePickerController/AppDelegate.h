//
//  AppDelegate.h
//  ZWTImagePickerController
//
//  Created by Chintan Dave on 13/10/15.
//  Copyright © 2015 Chintan Dave. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

