//
//  ViewController.m
//  ZWTImagePickerController
//
//  Created by Chintan Dave on 13/10/15.
//  Copyright © 2015 Chintan Dave. All rights reserved.
//

#import "ViewController.h"
#import "ZWTImagePickerController Source/ZWTImagePickerController.h"

@interface ViewController ()<ZWTImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imgvPreview;

@end

@implementation ViewController

#pragma mark - UIViewController Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Event Methods
- (IBAction)btnSelectTap:(UIButton *)sender
{
    ZWTImagePickerController *imagePicker = [[ZWTImagePickerController alloc] init];
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.delegate = self;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (IBAction)btncameraTap:(UIButton *)sender
{
    ZWTImagePickerController *imagePicker = [[ZWTImagePickerController alloc] init];
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

#pragma mark - ZWTImagePickerControllerDelegate Methods
- (void)imagePickerController:(ZWTImagePickerController *_Nonnull)picker didFinishPickingMedia:(NSArray *_Nonnull)media
{
    [self dismissViewControllerAnimated:YES completion:^
    {
        [UIView transitionWithView:self.view
                          duration:0.3f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^
        {
            self.imgvPreview.image = [media firstObject];
        } completion:nil];
    }];
}

- (void)imagePickerControllerDidCancel:(ZWTImagePickerController *_Nonnull)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end