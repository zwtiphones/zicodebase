//
//  AppDelegate.h
//  ZWTValidation
//
//  Created by Chintan Dave on 05/08/15.
//  Copyright (c) 2015 Zealousweb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

