//
//  main.m
//  ZWTValidation
//
//  Created by Chintan Dave on 05/08/15.
//  Copyright (c) 2015 Zealousweb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
