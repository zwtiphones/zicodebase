//
//  NSDate+ZWTDate.h
//  ZWTFoundation
//
//  Created by Chintan Dave on 14/08/15.
//  Copyright © 2015 Zealousweb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (ZWTDate)

+ (NSDate *)dateFromString:(NSString *)dateAsString withFormat:(NSString *)dateFormat;
+ (NSDate *)dateTimeFromeDate:(NSDate *)date andTime:(NSDate *)time;

- (NSString *)convertToStringWithFormat:(NSString *)dateFormat;

- (NSNumber *)ageInYears;

@end