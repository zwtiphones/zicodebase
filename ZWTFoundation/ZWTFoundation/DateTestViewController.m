//
//  ViewController.m
//  ZWTFoundation
//
//  Created by Chintan Dave on 05/08/15.
//  Copyright (c) 2015 Zealousweb. All rights reserved.
//

#import "DateTestViewController.h"
#import "NSDate+ZWTDate.h"

@interface DateTestViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblToday;

@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@property (weak, nonatomic) IBOutlet UILabel *lblDateTime;

@property (weak, nonatomic) IBOutlet UILabel *lblAge;

@end

@implementation DateTestViewController

@synthesize lblToday, lblDate, lblTime, lblDateTime, lblAge;

#pragma mark - UIViewController Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self testDateCategory];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Test Methods
- (void)testDateCategory
{
    NSString *today = [[NSDate date] convertToStringWithFormat:@"MMMM d, yyyy h:m a"];;
    
    lblToday.text = [NSString stringWithFormat:@"Today: %@", today];
    
    NSDate *date = [NSDate dateFromString:lblDate.text withFormat:@"dd MMM yyyy"];
    NSDate *time = [NSDate dateFromString:lblTime.text withFormat:@"hh:mm a"];
    
    NSDate *dateTime = [NSDate dateTimeFromeDate:date andTime:time];
    
    lblDateTime.text = [dateTime convertToStringWithFormat:@"MMMM d, yyyy h:m a"];
    
    NSString *birthDateString = @"3 Aug 1988";
    
    NSDate *birthDate = [NSDate dateFromString:birthDateString withFormat:@"dd MMM yyyy"];
    
    NSNumber *age = [birthDate ageInYears];
    
    lblAge.text = [NSString stringWithFormat:@"%@ years", [age stringValue]];
}

@end