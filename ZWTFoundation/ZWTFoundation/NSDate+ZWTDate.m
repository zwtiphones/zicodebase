//
//  NSDate+ZWTDate.m
//  ZWTFoundation
//
//  Created by Chintan Dave on 14/08/15.
//  Copyright © 2015 Zealousweb. All rights reserved.
//

#import "NSDate+ZWTDate.h"

@implementation NSDate (ZWTDate)

+ (NSDate *)dateFromString:(NSString *)dateAsString withFormat:(NSString *)dateFormat
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    NSLocale *enUSPosixLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    
    [formatter setLocale:enUSPosixLocale];
    [formatter setDateFormat:dateFormat];
    
    NSDate *date = [formatter dateFromString:dateAsString];
    
    return date;
}

+ (NSDate *)dateTimeFromeDate:(NSDate *)date andTime:(NSDate *)time
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    #if __IPHONE_OS_VERSION_MIN_REQUIRED >= 80000 // iOS 8.0 or later
    
        NSCalendarUnit dateUnit = NSCalendarUnitYear | NSCalendarUnitMonth  | NSCalendarUnitDay;
        NSCalendarUnit timeUnit = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;

    #else // less than iOS 8.0
    
        NSCalendarUnit dateUnit = NSYearCalendarUnit | NSMonthCalendarUnit  | NSDayCalendarUnit;
        NSCalendarUnit timeUnit = NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;

    #endif
    
    NSDateComponents *dateComponents = [calendar components:dateUnit fromDate:date];
    NSDateComponents *timeComponents = [calendar components:timeUnit fromDate:time];
    
    NSDateComponents *dateTimeComponents = [[NSDateComponents alloc] init];
    
    [dateTimeComponents setYear:dateComponents.year];
    [dateTimeComponents setMonth:dateComponents.month];
    [dateTimeComponents setDay:dateComponents.day];
    [dateTimeComponents setHour:timeComponents.hour];
    [dateTimeComponents setMinute:timeComponents.minute];
    
    NSDate *dateTime = [calendar dateFromComponents:dateTimeComponents];

    return dateTime;
}

- (NSString *)convertToStringWithFormat:(NSString *)dateFormat
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    NSLocale *enUSPosixLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    
    [formatter setLocale:enUSPosixLocale];
    [formatter setDateFormat:dateFormat];
    
    NSString *dateAsString = [formatter stringFromDate:self];
    
    return dateAsString;
}

- (NSNumber *)ageInYears
{
    NSDateComponents *ageComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear
                                                                      fromDate:self
                                                                        toDate:[NSDate date]
                                                                       options:0];
    NSInteger age = [ageComponents year];
    
    return @(age);
}

@end